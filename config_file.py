import pygame
pygame.font.init()

#font
font = pygame.font.Font('freesansbold.ttf', 100)
largeText = pygame.font.Font('freesansbold.ttf', 115)
font2 = pygame.font.Font('freesansbold.ttf', 65)


#screen

display_width = 1000
display_height = 800

screen = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('River Crossing')

#colors

Black = (0, 0, 0)
WHITE = (255, 255, 255)
Red = (255, 0, 0)
Green = (0, 255, 0)
blue = (0, 0, 255)
lime = ((180, 255, 100))

GREEN = (20, 255, 140)
GREY = (210, 210, 210)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
PURPLE = (255, 0, 255)
YELLOW = (255, 255, 0)
CYAN = (0, 255, 255)
BLUE = (100, 100, 255)

colorList = (RED, GREEN, PURPLE, YELLOW, CYAN, BLUE)

#message display


def crash():
   
    TextSurf, TextRect = text_objects('You crashed', largeText)
    TextRect.center = ((1000 / 2), (800 / 2))
    screen.blit(TextSurf, TextRect)

def reach():
    
    TextSurf, TextRect = text_objects('you reached!', largeText)
    TextRect.center = ((1000 / 2), (800 / 2))
    screen.blit(TextSurf, TextRect)

def text_objects(text,font):

    textSurface = font.render(text, True, Black)
    return textSurface, textSurface.get_rect()


#round

def Round1():
    text = 'Round1'
    Rounds(text)

def Round2():
    text = 'Round2'
    Rounds(text)

def Round3():
    text = 'Round3'
    Rounds(text)


def Rounds(text):

    screen.fill(Black)
    Round = font.render(text, True, WHITE)

    screen.blit(Round, (330, 350))

def gameover():
    screen.fill((240, 248, 255))
    game_over = font.render('GAME OVER', True, Black)
    screen.blit(game_over, (180, 80))

def winner1():
    text = "Winner : Player1"
    img = pygame.image.load("monkey.png")
    img = pygame.transform.scale(img, (300, 300))
    Winner(text,img)

def winner2():
    text = "Winner : player2"
    img = pygame.image.load("frog2.jpg")
    img = pygame.transform.scale(img, (300, 300))
    Winner(text,img)

def Winner(text,img):
    winner = font2.render(text, True, Black)
    screen.blit(winner, (190, 300))
    screen.blit(img, (350, 400))


    




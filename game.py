import pygame
import random
import time

from config_file import *
from player import Player
from obstacles import moving_obstacle
from obstacles import fixed_obstacle
pygame.init()

# group of all the sprites used in this game

all_sprites_list = pygame.sprite.Group()

# player objects(monkey,frog)

#monkey (player1)
x = "monkey.png"
player1 = Player(70, 60, 50, x)
player1.rect.x = (display_width * 0.45)
player1.rect.y = (display_height * 0.9)

#frog (player2)
x = "frog2.jpg"
player2 = Player(60, 50, 50, x)
player2.rect.x = (display_width * 0.45)
player2.rect.y = (display_height * 0)


# add the players to the sprite group
all_sprites_list.add(player1)
all_sprites_list.add(player2)


# start end
font = pygame.font.SysFont("comicsansms", 50)
start = font.render("START", True, (0, 0, 0))
end = font.render("END", True, (0, 0, 0))


score1 = 0
score2 = 0
#font = pygame.font.SysFont("comicsansms", 50)
Score1 = font.render('player1 : ' + str(score1), True, (0, 0, 0))
Score2 = font.render('player2 : ' + str(score2), True, (0, 0, 0))

# players turn
font = pygame.font.SysFont("comicsansms", 30)
turn1 = font.render("player1's turn", True, (0, 0, 0))
turn2 = font.render("player2's turn", True, (0, 0, 0))


# fixed_objects

# group of all the fixed objects
all_fixed_obs = pygame.sprite.Group()

for j in range(1, 5):
    for i in range(0, 5):
        fobs = fixed_obstacle(Black, 50, 50)
        fobs.rect.x = 80 + (i * 220)
        fobs.rect.y = 0 + (j * 150)
        all_fixed_obs.add(fobs)
        all_sprites_list.add(fobs)

# moving_objects

# 3 groups for moving obstacles (each for each round)
all_coming_obs1 = pygame.sprite.Group()
all_coming_obs2 = pygame.sprite.Group()
all_coming_obs3 = pygame.sprite.Group()


# for round1
for j in range(0, 6):
    for i in range(random.randint(1, 3), random.randint(5, 6)):
        mobs = moving_obstacle(random.choice(colorList), 80, 60, 20)
        mobs.rect.x = 0 + i * 250
        mobs.rect.y = 60 + j * 150

        # all_sprites_list.add(mobs)
        all_coming_obs1.add(mobs)

# for round2
for j in range(0, 6):
    for i in range(random.randint(1, 3), random.randint(5, 6)):
        mobs = moving_obstacle(random.choice(colorList), 80, 60, 50)
        mobs.rect.x = 0 + i * 250
        mobs.rect.y = 60 + j * 150

        # all_sprites_list.add(mobs)
        all_coming_obs2.add(mobs)

# for round3
for j in range(0, 6):
    for i in range(random.randint(1, 3), random.randint(5, 6)):
        mobs = moving_obstacle(random.choice(colorList), 80, 60, 80)
        mobs.rect.x = 0 + i * 250
        mobs.rect.y = 60 + j * 150

        # all_sprites_list.add(mobs)
        all_coming_obs3.add(mobs)


# crashed message

def message_display(text):

    if text == 'You crashed':
        crash()

    if text == 'you reached!':
        reach()

    pygame.display.flip()

    time.sleep(2)
    if player == player1:
        gameloop()

# function for message display after each round


def Round():

    if count == 0:
        Round1()
    elif count == 2:
        Round2()
    elif count == 4:
        Round3()

    pygame.display.flip()
    time.sleep(2)

    gameloop()

# function called after 3 rounds get over


def Gameover():

    global score1
    global score2

    global f
    global player
    global score1
    global score2
    global count

    gameover()

    if score1 > score2:
        winner1()
        score = "Score: " + str(score1)
        Score = font2.render(score, True, Black)
        screen.blit(Score, (320, 700))

    elif score2 > score1:

        score = "Score: " + str(score2)
        Score = font2.render(score, True, Black)
        screen.blit(Score, (320, 700))

    elif score2 == score1:
        if time1 > time2:
            winner1()
            score = "Score: " + str(score1)
            Score = font2.render(score, True, Black)
            screen.blit(Score, (320, 700))


        else:
            score = "Score: " + str(score2)
            Score = font2.render(score, True, Black)
            screen.blit(Score, (320, 700))
            

    print(time1)
    print(time2)
    pygame.display.update()
    time.sleep(4)

    speed = 1
    f = 1
    player = player1
    score1 = 0
    score2 = 0
    count = 0
    round_score1 = 0
    round_score2 = 0
    Round()
    gameloop()


speed = 1
f = 1
player = player1
#score1 = 0
#score2 = 0
count = 0

time1 = 0
time2 = 0


player = player1
# game


def gameloop():

    global f
    global player
    global score1
    global score2
    global count
    global time1
    global time2

    round_score1 = 0
    round_score2 = 0

    round_time1 = 0
    round_time2 = 0

    if f == 1:
        f = 0
        player = player1
    else:
        if player == player1:
            player = player2
        else:
            player = player1

    clock = pygame.time.Clock()

    player1.rect.x = (display_width * 0.45)
    player1.rect.y = (display_height * 0.92)

    player2.rect.x = (display_width * 0.45)
    player2.rect.y = (display_height * 0)

    xf = 0
    xm = 0
    f = 0

    q = False

    count = count + 1
    if count == 1 or count == 2:
        all_coming_obs = all_coming_obs1
    elif count == 3 or count == 4:
        all_coming_obs = all_coming_obs2
    elif count == 5 or count == 6:
        all_coming_obs = all_coming_obs3

    while not q:

        if player == player1:
            round_time1 = round_time1 + 1

        if player == player2:
            round_time2 = round_time2 + 1

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                q = True
                quit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            if player.rect.x >= 0:
                player.moveLeft(5)
        if keys[pygame.K_RIGHT]:
            if player.rect.x <= 930:
                player.moveRight(5)

        if keys[pygame.K_UP]:
            if player.rect.y >= 2:
                player.moveUp(5)
                xm = xm + 5
                xf = xf + 5

        if keys[pygame.K_DOWN]:
            if player.rect.y <= 750:
                player.moveDown(5)
                xm = xm - 5
                xf = xf - 5

        if xm >= 50 and xf >= 200:
            xf = 0
            score1 = score1 + 5

            print(score1)

        if xm >= 150:
            score1 = score1 + 10

            xm = 0
            print(score1)

        if xm <= -150:
            score2 = score2 + 10

            xm = 0

        if xm <= -50 and xf <= -200:
            xf = 0
            score2 = score2 + 5

        screen.fill(WHITE)

        x = 0
        for i in range(1, 7):
            pygame.draw.rect(screen, lime, [0, 0 + x, 1000, 50])
            x += 150

        if player == player1:
            screen.blit(turn1, (850, 10))
        else:
            screen.blit(turn2, (850, 10))

        for obs in all_coming_obs:
            obs.move(speed)
            if obs.rect.x > display_width:
                obs.repaint(random.choice(colorList))
                obs.rect.x = 0

        collision_list = pygame.sprite.spritecollide(
            player, all_coming_obs, False)
        for obs in collision_list:
            print("crash")
            if player == player1:
                message_display('You crashed')
                time1 = time1 + round_time1

            else:
                message_display('You crashed')
                time2 = time2 + round_time2

                if count == 6:
                    Gameover()
                else:
                    Round()

        fixed_collision_list = pygame.sprite.spritecollide(
            player, all_fixed_obs, False)
        for obs in fixed_collision_list:
            print("fixed crash")
            if player == player1:
                time1 = time1 + round_time1

                message_display('You crashed')

            else:
                time2 = time2 + round_time2

                message_display('You crashed')
                if count == 6:
                    Gameover()
                else:
                    Round()

        all_sprites_list.draw(screen)
        all_coming_obs.draw(screen)

        if player == player1:
            timer = font.render('Time : ' + str(round_time1), True, (0, 0, 0))
            screen.blit(timer, (200, 10))

        if player == player2:
            timer = font.render('Time : ' + str(round_time2), True, (0, 0, 0))
            screen.blit(timer, (200, 10))

        Score1 = font.render('player1 : ' + str(score1), True, (0, 0, 0))
        Score2 = font.render('player2 : ' + str(score2), True, (0, 0, 0))
        screen.blit(Score1, (800, 780))
        screen.blit(Score2, (800, 750))

        if player == player1:
            if player1.rect.y <= 1:
                score1 = score1 + 10
                time1 = time1 + round_time1

                message_display('you reached!')
            screen.blit(start, (0, 800 - start.get_height()))
            screen.blit(end, (0, 50 - end.get_height()))
        elif player == player2:
            if player.rect.y >= 745:

                score2 = score2 + 10
                time2 = time2 + round_time2

                message_display('you reached!')
                if count == 6:
                    Gameover()
                else:
                    Round()
            screen.blit(end, (0, display_height - 40))
            screen.blit(start, (0, 0))

        if player == player1:
            screen.blit(start, (0, 800 - start.get_height()))
            screen.blit(end, (0, 50 - end.get_height()))
        elif player == player2:
            screen.blit(end, (0, display_height - 40))
            screen.blit(start, (0, 0))

        pygame.display.update()
        clock.tick(60)


Round()
gameloop()
pygame.quit()
quit()
screen.fill()
